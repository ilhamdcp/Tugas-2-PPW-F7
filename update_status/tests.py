import environ
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from update_status.csui_helper import get_access_token, get_client_id, verify_user, get_data_user
from .views import index, add_status, response
from .models import Account, Status
from .forms import Status_Form

# Create your tests here.
root = environ.Path(__file__) - 3  # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False), )
environ.Env.read_env('.env')


class UpdateStatusUnitTest(TestCase):
    def setUp(self):
        self.username = env('SSO_USERNAME')
        self.password = env('SSO_PASSWORD')

    def test_update_status_url_is_exist(self):
        response = Client().get('/update-status/')
        self.assertEqual(response.status_code, 200)

    def test_update_status_using_index_func(self):
        found = resolve('/update-status/')
        self.assertEqual(found.func, index)

    def test_user_login_or_not(self):
        not_logged_in = self.client.get('/update-status/')
        self.assertIn('SSO', not_logged_in.content.decode('utf8'))
        self.assertTemplateUsed(not_logged_in, 'login.html')
        found = self.client.post('/update-status/custom_auth/login/',
                                 {'username': self.username, 'password': self.password})
        form = self.client.get('/update-status/')

        self.assertIn('status', form.content.decode('utf8'))

    def test_add_status(self):
        invalid_form = self.client.get('/update-status/add_status/', {'message': 'saya mau coba', 'name':'ilham.darmawan'})
        self.assertEqual(invalid_form.status_code, 302)
        acc = Account(kode_identitas='1606882351', nama='ilham.darmawan')
        acc.save()
        response['name'] = 'ilham.darmawan'
        login = self.client.post('/update-status/custom_auth/login/',
                                 {'username': self.username, 'password': self.password})
        valid_form = self.client.post('/update-status/add_status/', {'message': 'saya mau coba', 'name':'ilham.darmawan'})
        self.assertEqual(valid_form.status_code, 302)

    def test_form_status_has_placeholder_and_css_classes(self):
        form = Status_Form()
        self.assertIn('class="form-control"', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'name': '', 'message': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['message'],
            ["This field is required."]
        )

    def test_update_status_post_fail(self):
        response = Client().post('/update-status/add_status', {'name': 'Team 7', 'message': ''})
        self.assertEqual(response.status_code, 302)

    # csui_helper test
    def test_get_access_token(self):
        username = "Ilham Darmawan"
        password = "decepe"

        test_1 = get_access_token(username, password)
        self.assertTrue(type(test_1), str)
        self.assertIsNone(test_1)
        self.assertRaises(Exception, get_access_token(username, password))

        test_2 = get_access_token(self.username, self.password)
        self.assertIsNotNone(test_2)

    def test_get_client_id(self):
        client_id = get_client_id()
        self.assertTrue(type(client_id), str)
        self.assertEqual(client_id, "X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG")

    def test_verify_id(self):
        verified = verify_user("1234567890")
        self.assertIn("error_description", verified)

    def test_get_data_user(self):
        access_token = '123456789'
        id = 'decepe'
        data_user = get_data_user(access_token, id)
        self.assertIn("detail", data_user)
        self.assertEqual(data_user["detail"], "Authentication credentials were not provided.")

    def test_isi_message_sesuai(self):
        statusnya = Status(message="apakah pesan ini sama?")
        self.assertEqual(str(statusnya), "apakah pesan ini sama?")

    # custom_auth test
    def test_auth_login(self):
        login_1 = self.client.post('/update-status/custom_auth/login/')
        self.assertEqual(login_1.status_code, 302)
        self.assertRaisesMessage(login_1, "Username atau password salah")
        login_1 = self.client.get('/update-status/custom_auth/login/')
        self.assertRaisesMessage(login_1, "Username atau password salah")

        login_2 = self.client.post('/update-status/custom_auth/login/',
                                   {'username': self.username, 'password': self.password})
        self.assertRaisesMessage("status", login_2)
        self.assertEqual(login_2.status_code, 302)

    def test_auth_logout(self):
        login = self.client.post('/update-status/custom_auth/login/',
                                 {"username": self.username, "password": self.password})
        logout = self.client.get('/update-status/custom_auth/logout/')
        check = self.client.post('/update-status/')
        self.assertEqual(logout.status_code, 302)
        self.assertRaisesMessage("Halaman Login", logout.content.decode('utf8'))
        self.assertIn("akun SSO", check.content.decode('utf8'))
