from django.conf.urls import url

from update_status.custom_auth import auth_logout, auth_login
from .views import add_status, index

#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_status', add_status, name='add_status'),
    url(r'^custom_auth/login/$', auth_login, name='auth_login'),
    url(r'^custom_auth/logout/$', auth_logout, name='auth_logout'),
]
