from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status, Account
from django.utils import timezone

# Create your views here.
response = {'author': 'Kelompok 7 PPW-F', 'status_form': Status_Form, 'logged_in': False}


def index(request):
    if 'user_login' in request.session:
        html = 'update_status.html'
        kode_identitas = request.session['kode_identitas']
        if (kode_identitas not in Account.objects.filter(kode_identitas=kode_identitas)):
            create_new_user(request)
        response['name'] = Account.objects.get(kode_identitas=kode_identitas).nama
        allStatus = Status.objects.filter(account=Account.objects.get(kode_identitas=kode_identitas))
        response['allStatus'] = allStatus
        response['npm'] = kode_identitas;
    else:
        html = 'login.html'
    return render(request, html, response)


def add_status(request):
    form = Status_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['message'] = request.POST['message']
        kode_identitas = get_data_user(request, 'kode_identitas')
        account = Account.objects.get(kode_identitas=kode_identitas) or None
        if account is not None:
            status = Status(account=account, name=response['name'], message=response['message'])
            status.save()
            allStatus = Status.objects.all()
            response['allStatus'] = allStatus
            html = 'update_status.html'
        return HttpResponseRedirect('/update-status/')
    else:
        return HttpResponseRedirect('/update-status/')



def get_data_user(request, tipe):
    data = None
    if tipe == "user_login" and 'user_login' in request.session:
        data = request.session['user_login']
    elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
        data = request.session['kode_identitas']

    return data


def create_new_user(request):
    nama = get_data_user(request, 'user_login')
    kode_identitas = get_data_user(request, 'kode_identitas')

    pengguna = Account()
    pengguna.kode_identitas = kode_identitas
    pengguna.nama = nama
    pengguna.created_at = timezone.now()
    pengguna.save()

    return pengguna
