from django.db import models


class Account(models.Model):
    kode_identitas = models.CharField('Kode Identitas', max_length=20, primary_key=True, )
    nama = models.CharField(max_length=200, default="NULL")
    created_at = models.DateTimeField(auto_now_add=True)


class Status(models.Model):
    account = models.ForeignKey(Account)
    name = models.CharField(max_length=27)
    message = models.TextField(max_length=300)
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.message
