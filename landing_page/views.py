from django.shortcuts import render
from halaman_profile.models import Profile
from update_status.views import response as r


# Create your views here.
def index(request):
    response = {'author': 'Kelompok 7 PPW-F', 'name': Profile.name}
    html = 'LandingPage.html'
    return render(request, html, response)
