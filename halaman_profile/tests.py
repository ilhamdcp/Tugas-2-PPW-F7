from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from update_status.models import Account
from .api_riwayat import get_riwayat
import environ

# Create your tests here.
root = environ.Path(__file__) - 3  # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False), )
environ.Env.read_env('.env')


class ProfileUnitTest(TestCase):
    def setUp(self):
        self.username = env('SSO_USERNAME')
        self.password = env('SSO_PASSWORD')

    def test_using_index_function(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)

    def test_index_when_loggin_and_not(self):
        index_redirect = self.client.get('/profile/')
        self.assertEqual(index_redirect.status_code, 302)
        acc = Account(kode_identitas='1606882351', nama='ilham.darmawan')
        acc.save()
        login = self.client.post('/update-status/custom_auth/login/',
                                 {'username': self.username, 'password': self.password})
        self.client.session['kode_identitas'] = '1606882351'
        index_render = self.client.get('/profile/')
        self.assertEqual(index_render.status_code, 200)
