from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from .models import Profile, Expertise, Profile_linkedin
from update_status.views import response as r, create_new_user
from update_status.models import Account, Status
from update_status.forms import Status_Form
from .api_riwayat import get_riwayat

response = {'status_form': Status_Form}


def index(request):
    if r['logged_in']:
        response['author'] = r['author']
        response['name'] = Profile.name
        response['email'] = Profile.email
        response['expertise'] = Expertise.expert
        response['npm'] = Profile.kode_identitas
        response['riwayat'] = get_riwayat().json()

        kode_identitas = request.session['kode_identitas']
        if (kode_identitas not in Account.objects.filter(kode_identitas=kode_identitas)):
            create_new_user(request)

        response['latestStatus'] =''
        response['latestStatusDate'] = ''
        response['jumlahAllStatus'] = 0
        response['name'] = Account.objects.get(kode_identitas=kode_identitas).nama
        allStatus = Status.objects.filter(account=Account.objects.get(kode_identitas=kode_identitas))
        response['allStatus'] = allStatus
        response['jumlahAllStatus'] = allStatus.count()
        response['latestStatus'] = allStatus.order_by('-created_date')[0].message if len(allStatus) > 0 else 'No message'
        response['latestStatusDate'] = allStatus.order_by('-created_date')[0].created_date if len(allStatus) > 0 else 'No message'

        return render(request, 'profile.html', response)
    else:
        return HttpResponseRedirect('/update-status/')

def edit_profile(request):
    if 'user_login' in request.session:
        response['name'] = Profile.name
        html = 'edit_profile.html'
        return render(request, html, response)

def save_to_database(request):
    if(request.method == 'POST'):
        firstName = request.POST['firstName']
        lastName = request.POST['lastName']
        imageUrl = request.POST['imageUrl']
        email = request.POST['email']
        profileUrl = request.POST['profileUrl']
        kode_identitas = request.session['kode_identitas']
        exist = Profile_linkedin.objects.filter(kode_identitas=kode_identitas).exists()
        if not exist:
            user = Profile_linkedin(firstName=firstName, lastName=lastName, imageUrl=imageUrl, email=email, profileUrl=profileUrl, kode_identitas=kode_identitas)
            user.save()
        return JsonResponse({'save':'ok'})


