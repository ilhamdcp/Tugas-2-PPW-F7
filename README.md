# Tugas PPW-2 
### Melanjutkan dari Tugas PPW-1
## Kelompok 7 (Kakashi)

* Ilham Darmawan C. P.
* Ivan Abdurrahman
* Muhammad Dimas Praharsa 

[![pipeline status](https://gitlab.com/ilhamdcp/tugas-1-ppw/badges/master/pipeline.svg)](https://gitlab.com/ilhamdcp/tugas-1-ppw/commits/master)
[![coverage report](https://gitlab.com/ilhamdcp/tugas-1-ppw/badges/master/coverage.svg)](https://gitlab.com/ilhamdcp/tugas-1-ppw/commits/master)

Our link herokuapp:
http://tugas-2-lingin.herokuapp.com/

<br/>
## Tulis catatan di sini ya!
